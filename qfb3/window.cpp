#include "window.h"
#include <QPushButton>

Window::Window(QWidget *parent) :
    QWidget(parent)
{
    setFixedSize(200, 100);

    m_button = new QPushButton("Qt PushButton", this);
    m_button->setGeometry(10, 10, 80, 30);
}
