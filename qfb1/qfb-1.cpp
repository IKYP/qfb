#include <QApplication>
#include <QPushButton>

int main(int argc, char **argv)
{
        QApplication app(argc, argv) ;

        QPushButton button;
        button.setText("Button Custom Test");
        button.setToolTip("This is a Tip!");
        button.show();

        return app.exec();
}
