#include <QApplication>
#include <QPushButton>

int main(int argc, char **argv)
{
        QApplication app(argc, argv) ;

        QPushButton button1 ("First");
        QPushButton button2 ("Second", &button1);
        button1.show();

        return app.exec();
}
